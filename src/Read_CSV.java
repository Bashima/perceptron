import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Read_CSV {

	public ArrayList<DataClass> rows;
	public ArrayList<DataClass> rowsTest;

	public int rowCount;
	public int featureCount;

	Read_CSV() {
		rows = new ArrayList<DataClass>();
		rowsTest = new ArrayList<DataClass>();
		rowCount = 0;
		featureCount = 0;
	}

	public ArrayList<DataClass> readScanner(String filename)
			throws FileNotFoundException {
		Scanner scn = new Scanner(new File(filename));
		scn.nextInt();
		featureCount = scn.nextInt();
		rowCount = scn.nextInt();

		while (scn.hasNext()) {
			DataClass temp = new DataClass();
			for (int i = 0; i < featureCount; i++) {
				temp.features[i] = scn.nextDouble();
				System.out.print(temp.features[i] + " ");
			}
			temp.classes = scn.nextInt();
			temp.dimension = featureCount;
			System.out.print(temp.classes + " ");
			System.out.println("");
			rows.add(temp);

		}
		scn.close();
		return rows;
	}

	public ArrayList<DataClass> readScannerTest(String filename)
			throws FileNotFoundException {
		Scanner scn = new Scanner(new File(filename));
		scn.nextInt();
		featureCount = scn.nextInt();
		rowCount = scn.nextInt();
		while (scn.hasNext()) {
			DataClass temp = new DataClass();
			for (int i = 0; i < featureCount; i++) {
				temp.features[i] = scn.nextDouble();
				System.out.print(temp.features[i] + " ");
			}
			temp.classes = scn.nextInt();
			temp.dimension = featureCount;
			System.out.print(temp.classes + " ");
			System.out.println("");
			rowsTest.add(temp);

		}
		// System.err.println("jjasdh: "+ rows.size());
		scn.close();
		return rowsTest;
	}

	// public ArrayList<double[]> read(String filename)
	// {
	//
	// BufferedReader br = null;
	// String line = "";
	// String cvsSplitBy = "\t";
	//
	// try {
	// br = new BufferedReader(new FileReader(filename));
	// line = br.readLine();
	// while ((line = br.readLine()) != null) {
	// // System.out.println(line);
	// String[] columnString = line.split(cvsSplitBy);
	// featureCount = columnString.length;
	// double [] column = new double[featureCount];
	// // System.out.println(columnCount);
	// for(int i=0; i<columnString.length; i++)
	// {
	// column[i] = Double.parseDouble(columnString[i]);
	// }
	// rows.add(column);
	// rowCount++;
	// }
	//
	// } catch (FileNotFoundException e) {
	// System.out.println("No file found");
	// e.printStackTrace();
	// } catch (IOException e) {
	// System.out.println("IO Exception");
	// e.printStackTrace();
	// } finally {
	// if (br != null) {
	// try {
	// br.close();
	// } catch (IOException e) {
	// System.out.println("IO Exception");
	// e.printStackTrace();
	// }
	// }
	// }
	// return rows;
	// }

	public void print() {
		System.out.println(rows.toString());
	}
}
