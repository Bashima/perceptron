import java.io.File;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Perceptron_kesslar {
	public ArrayList<double[]> data;
	public ArrayList<double[]> dataTest;
	public ArrayList<double[]> dataExtended;
	public ArrayList<double[]> dataExtendedTest;
	public double[] weights;
	public double p;
	public int l, m, n, dimention, countExtended, countExtendedTest, error;

	Perceptron_kesslar() {
		data = new ArrayList<double[]>();
		dataTest = new ArrayList<double[]>();
		dataExtended = new ArrayList<double[]>();
		dataExtendedTest = new ArrayList<double[]>();
		p = 0.006;
		l = 0;
		m = 0;
		n = 0;
		dimention = 0;
		countExtended = 0;
		countExtendedTest = 0;
		error = 0;
		weights = new double[100];
	}

	public void fileReader(String fileName) throws Exception {
		Scanner scn = new Scanner(new File(fileName));
		l = scn.nextInt();
		m = scn.nextInt();
		n = scn.nextInt();
		while (scn.hasNext()) {
			double arr[] = new double[l + 1];
			for (int i = 0; i <= l; i++) {
				arr[i] = scn.nextDouble();
				// System.out.print(arr[i] + " ");
			}
			data.add(arr);
			// System.out.println();
		}
		scn.close();

	}

	public void fileReaderTest(String fileName) throws Exception {
		Scanner scn = new Scanner(new File(fileName));
		l = scn.nextInt();
		m = scn.nextInt();
		n = scn.nextInt();
		while (scn.hasNext()) {
			double arr[] = new double[l + 1];
			for (int i = 0; i <= l; i++) {
				arr[i] = scn.nextDouble();
				// System.out.print(arr[i] + " ");
			}
			dataTest.add(arr);
			// System.out.println();
		}
		scn.close();

	}

	public void extendedDimension() {
		countExtended = 0;
		dimention = (l + 1) * m;
		for (int i = 0; i < data.size(); i++) {
			int cl = (int) data.get(i)[l];
			for (int j = 1; j <= m; j++) {
				if (cl == j) {

				} else {
					double[] dt = new double[dimention + 1];
					int ind = 1;
					for (int k = 1; k <= m; k++) {
						if (k == cl) {
							for (int s = 1; s <= l; s++)
								dt[ind++] = data.get(i)[s];
							dt[ind++] = 1;
						} else if (k == j) {
							for (int s = 1; s <= l; s++)
								dt[ind++] = -data.get(i)[s];
							dt[ind++] = -1;

						} else {
							for (int s = 1; s <= l + 1; s++)
								dt[ind++] = 0;
						}
					}
					dataExtended.add(dt);
					countExtended++;
				}

			}

		}

		// for (double[] rows : dataExtended) {
		// for (int i = 0; i < dimention; i++) {
		// // System.out.print(rows[i] + " ");
		// }
		// // System.out.println();
		// }
	}

	public void extendedDimensionTest() {
		countExtendedTest = 0;
		dimention = (l + 1) * m;
		for (int i = 0; i < dataTest.size(); i++) {
			int cl = (int) dataTest.get(i)[l];
			for (int j = 1; j <= m; j++) {
				if (cl == j) {

				} else {
					double[] dt = new double[dimention + 1];
					int ind = 1;
					for (int k = 1; k <= m; k++) {
						if (k == cl) {
							for (int s = 1; s <= l; s++)
								dt[ind++] = dataTest.get(i)[s];
							dt[ind++] = 1;
						} else if (k == j) {
							for (int s = 1; s <= l; s++)
								dt[ind++] = -dataTest.get(i)[s];
							dt[ind++] = -1;

						} else {
							for (int s = 1; s <= l + 1; s++)
								dt[ind++] = 0;
						}
					}
					dataExtendedTest.add(dt);
					countExtendedTest++;
				}

			}

		}

		// for (double[] rows : dataExtended) {
		// for (int i = 0; i < dimention; i++) {
		// // System.out.print(rows[i] + " ");
		// }
		// // System.out.println();
		// }
	}

	public void kessler() {
		int rangeMin = 0;
		int rangeMax = 1;
		for (int i = 1; i <= dimention; i++) {
			Random r = new Random();
			double randomValue = rangeMin + (rangeMax - rangeMin)
					* r.nextDouble();
			weights[i] = randomValue;
		}
		for (int i = 1; i <= 1000; i++) {
			ArrayList<double[]> errorData = new ArrayList<double[]>();
			// error=0;
			for (int j = 0; j < dataExtended.size(); j++) {
				double ret = 0;
				double[] dt = dataExtended.get(j);
				for (int k = 1; k <= dimention; k++) {
					ret += weights[k] * dt[k];
				}
				if (ret <= 0) {
					errorData.add(dt);
				}

			}

			if (errorData.size() == 0)
				break;
			for (int j = 0; j < errorData.size(); j++) {
				double[] dt = new double[100];
				dt = errorData.get(j);
				for (int k = 1; k <= dimention; k++) {
					weights[k] -= dt[k] * p;

				}

			}

		}

		// /testing
		
//		for (int i = 0; i < dataTest.size(); i++) {
//			double[] dtt = new double[100];
//			dtt = dataTest.get(i);
//			double ret = 0;
//			double ret1 = weights[1]* dtt[1];
//			double ret2 = weights[2]* dtt[2];
//			double ret3 = weights[3]* dtt[3];
//			System.out.print(i+": ");
//			if(ret1>ret2)
//			{
//				if(ret1>ret3)
//				{	
//					System.out.print("1 ");
//				}
//				else
//				{
//					System.out.print("3 ");
//				}
//			}
//			else
//			{
//				if(ret2>ret3)
//				{	
//					System.out.print("2 ");
//				}
//				else
//				{
//					System.out.print("2 ");
//				}
//			}
//			System.out.println(dtt[dtt.length-1]);
		
		
		
		for (int i = 0; i < dataExtendedTest.size(); i++) {
			double[] dtt = new double[100];
			dtt = dataExtendedTest.get(i);
			double ret = 0;
			for (int k = 1; k <= dimention; k++) {
				ret += weights[k] * dtt[k];
			}
//			System.out.println(dataExtendedTest.size());
			if (ret <= 0) {
				this.error++;
				System.out.println(dataExtendedTest.size()/2);
			}

		}

	}

	public static void main(String args[]) throws Exception {
		String fileNameTrain = "Train.txt";
		String fileNameTest = "Train.txt";
		Perceptron_kesslar pk = new Perceptron_kesslar();
		pk.fileReader(fileNameTrain);
		pk.fileReaderTest(fileNameTest);
		pk.extendedDimension();
//		pk.extendedDimensionTest();
		pk.kessler();
		System.out.println("Error: " + pk.error);
	}
}
