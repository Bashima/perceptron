import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;

public class Perceptron_normal {
	public ArrayList<DataClass> data;
	public ArrayList<DataClass> dataTest;
	public ArrayList<Level> levels;
	public double p;
	public int featureCount;
	public int tempClass;
	public int columnSize;
	public int correctValue;
	public double[] bestWeights;
	public int errortest, flag;
	public int dimension;
	public int O=-4000;

	Perceptron_normal(int featureC) {
		levels = new ArrayList<Level>();
		bestWeights = new double[featureC];
		data = new ArrayList<DataClass>();
		dataTest = new ArrayList<DataClass>();
		featureCount = featureC;
		p = 0.5;
		columnSize = 0;
		correctValue = 0;
		errortest = 0;
		flag = 0;
		dimension = 0;
	}

	@SuppressWarnings("unchecked")
	public void Perceptron_tiling() {
		ArrayList<DataClass> upperClass = new ArrayList<DataClass>();
		ArrayList<DataClass> lowerClass = new ArrayList<DataClass>();
		while (true) {
			Level l = new Level();
			l.w1 = Perceptron_pocket(data, dimension);
			if (flag == 1) {
				l.flag = true;
				levels.add(l);
				break;

			}
			for (DataClass row : data) {
				double ret = Find_class(row, l.w1, dimension);
				if (ret > 0) {
					tempClass = 1;
					upperClass.add(row);
				} else {
					tempClass = 2;
					lowerClass.add(row);
				}
			}
			l.w2 = Perceptron_pocket(upperClass, dimension);
			l.w3 = Perceptron_pocket(lowerClass, dimension);

			ArrayList<DataClass> newData = new ArrayList<DataClass>();
			for (DataClass ds : data) {
				DataClass newDs = new DataClass();
				;
				double ret1 = Find_class(ds, l.w1, dimension);
				if (ret1 > 0) {
					tempClass = 1;
				} else {
					tempClass = 2;
				}
				if (tempClass == ds.classes) {
					newDs.features[0] = 1;
				} else {
					newDs.features[0] = 0;
				}

				double ret2 = Find_class(ds, l.w2, dimension);
				if (ret2 > 0) {
					tempClass = 1;
				} else {
					tempClass = 2;
				}
				if (tempClass == ds.classes) {
					newDs.features[1] = 1;
				} else {
					newDs.features[1] = 0;
				}

				double ret3 = Find_class(ds, l.w3, dimension);
				if (ret3 > 0) {
					tempClass = 1;
				} else {
					tempClass = 2;
				}
				if (tempClass == ds.classes) {
					newDs.features[2] = 1;
				} else {
					newDs.features[2] = 0;
				}

				newData.add(ds);
			}
			levels.add(l);
			data.clear();
			data = (ArrayList<DataClass>) newData.clone();
			dimension = 3;

			// test
			// int errorCount = 0;
			for (DataClass d : dataTest) {
				double ret1, ret2, ret3;
				int a1, a2, a3;
				int nowClass = 0;
				for (Level lev : levels) {
					if (lev.flag == false) {
						ret1 = Find_class(d, lev.w1, dimension);
						if (ret1 > 0) {
							nowClass = 1;
						} else {
							nowClass = 2;
						}
						if (nowClass != d.classes) {
							a1 = 0;
						} else {
							a1 = 1;
						}
						
						ret2 = Find_class(d, lev.w2, dimension);
						if (ret2 > 0) {
							nowClass = 1;
						} else {
							nowClass = 2;
						}
						if (nowClass != d.classes) {
							a2 = 0;
						} else {
							a2 = 1;
						}
						ret3 = Find_class(d, lev.w3, dimension);
//						System.out.println("here"+ret3);
						if (ret3 > O) {
							nowClass = 1;
						} else {
							nowClass = 2;
						}
						if (nowClass != d.classes) {
							a3 = 0;
						} else {
							a3 = 1;
						}

						DataClass dt = new DataClass();
						dt.features[0] = a1;
						dt.features[1] = a2;
						dt.features[2] = a3;

						dt.classes = d.classes;
						d = dt;
					} else {
						
						ret1 = Find_class(d, lev.w1, dimension);
						if (ret1 > 0) {
							nowClass = 1;
						} else {
							nowClass = 2;
						}
						// System.err.println("error: " + errorCount);
						// if (nowClass != d.classes)
						// errorCount++;
					}

				}
				System.err.println("class: " + nowClass);
			}

			// System.err.println("error: " + errorCount);

		}

	}

	public double[] Perceptron_pocket(ArrayList<DataClass> data, int dimension) {
		double[] weights = new double[100];
		int rangeMin = 0;
		int rangeMax = 1;
		for (int i = 0; i < dimension; i++) {
			Random r = new Random();
			double randomValue = rangeMin + (rangeMax - rangeMin)
					* r.nextDouble();
			weights[i] = randomValue;
			// weights[i] = 0;
			bestWeights[i] = weights[i];
			// System.out.println(randomValue);
		}
		for (int loop = 0; loop < 1000; loop++) {
			for (int i = 0; i < dimension; i++) {
				// System.out.println(weights[i]);
			}
			// System.out.println();
			int errorCount = 0;
			int rightCount = 0;
			ArrayList<DataClass> errorList = new ArrayList<DataClass>();
			for (DataClass row : data) {
				double ret = Find_class(row, weights, dimension);
				if (ret > 0) {
					tempClass = 1;
				} else {
					tempClass = 2;
				}
				if (tempClass != row.classes) {
					errorList.add(row);
					errorCount++;
				} else {
					rightCount++;
				}
			}

			if (errorCount == 0) {
				flag = 1;
				break;
			}
			// else {
			double[] sum = new double[100];
			for (DataClass row : errorList) {
				if (row.classes == 2) {
					for (int i = 0; i < dimension; i++) {
						sum[i] += p * row.features[i];
					}
					sum[dimension] += p;
				} else {
					for (int i = 0; i < dimension; i++) {
						sum[i] -= p * row.features[i];
					}
					sum[dimension] -= p;
				}
			}

			if (rightCount > correctValue) {
				correctValue = rightCount;
				for (int i = 0; i < dimension; i++) {
					bestWeights[i] = weights[i];
				}
			}
			for (int i = 0; i <= dimension; i++) {
				weights[i] -= sum[i];
			}
			// weights[columnSize-1] -= p;
			if (errorCount == 0) {
				flag = 1;
				break;
			}
			// }
		}
		// int index = 0;
		// for (double[] rowTest : dataTest) {
		// double ret = Find_class(rowTest);
		// if (ret > 0)
		// tempClass = 1.0;
		// else
		// tempClass = 2.0;
		//
		// if (tempClass != rowTest[columnSize - 1]) {
		// for (int i = 0; i < columnSize; i++) {
		// System.out.print(" " + rowTest[i]);
		// }
		//
		// System.out.println("\nhere: " + tempClass + " "
		// + rowTest[columnSize - 1] + " " + ret + " "+ index);
		// errortest++;
		// }
		// index++;
		// }
		// System.out.println("Error: " + errortest);
		return weights;
	}

	public double Find_class(DataClass x, double[] weights, int dimension) {
		double d = 0;

		for (int i = 0; i < dimension; i++) {
			d += weights[i] * x.features[i];
			// System.err.println("lenght: "+ x.features.length+ " weight: "+
			// weights[i] );
		}
		// System.err.println(dimension);
		d += weights[dimension];

		// System.err.println("lenght: "+ x.features.length+ " weight: "+
		// weights.toString());
		return d;
	}

	@SuppressWarnings("unchecked")
	public static void main(String args[]) {
		String filename = "trainTiling.txt";
		Read_CSV read = new Read_CSV();
		ArrayList<DataClass> temp = new ArrayList<DataClass>();
		try {
			temp = (ArrayList<DataClass>) read.readScanner(filename).clone();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Read_CSV read1 = new Read_CSV();
		String testFileName = "testTiling.txt";
		ArrayList<DataClass> tempTest = new ArrayList<DataClass>();
		try {
			tempTest = (ArrayList<DataClass>) read1.readScannerTest(
					testFileName).clone();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Perceptron_normal p = new Perceptron_normal(
				(int) temp.get(0).features.length);
		p.data = (ArrayList<DataClass>) temp.clone();
		// System.err.println("length: " + p.data.size());
		p.dimension = p.data.get(0).dimension;
		p.dataTest = (ArrayList<DataClass>) tempTest.clone();
		// System.err.println("length: " + p.dataTest.size());
		// p.Perceptron_pocket();
		p.Perceptron_tiling();
	}
}
